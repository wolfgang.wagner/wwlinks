# wwlinks

This is a simple TYPO3 template for usage as a linktree alternative.

## Installation

1. Install the extension as usual via composer or extension manager
2. Mark a page as root page for a new website
3. Create/open the site configuration for this site
4. Include the set "Template for a Linktree alternative"
5. In the page properties of this page activate the 1 column backend layout

After that, you can set the links to your social media profiles in the site settings editor.

To create a link, add a content element of type "Header" on the page.
