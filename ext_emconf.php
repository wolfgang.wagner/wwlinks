<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Template for a linktree alternative',
    'description' => '',
    'category' => 'templates',
    'author' => 'Wolfgang Wagner',
    'author_email' => 'wwagner@wwagner.net',
    'state' => 'stable',
    'version' => '2.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '13.4.2-13.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
